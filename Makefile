clean_postgres:
	sudo rm -rf ~/.papertext/postgresql/

clean_neo4j:
	sudo rm -rf ~/.papertext/neo4j/

clean: clean_postgres clean_neo4j

setup_postgres:
	mkdir -p ~/.papertext/postgresql/

setup_neo4j:
	mkdir -p ~/.papertext/neo4j
	mkdir ~/.papertext/neo4j/conf
	mkdir ~/.papertext/neo4j/data
	mkdir ~/.papertext/neo4j/import
	mkdir ~/.papertext/neo4j/logs
	mkdir ~/.papertext/neo4j/plugins

setup: clean setup_postgres setup_neo4j

fix_paperback:
	cd ./paperback/ && poetry run fix

fix_papertext_auth:
	cd ./papertext_auth/ && poetry run fix

fix_papertext_docs:
	cd ./papertext_docs/ && poetry run fix

fix: fix_paperback fix_papertext_auth fix_papertext_docs
